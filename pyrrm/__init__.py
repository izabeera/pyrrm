__version__ = "0.1.0"

import numpy as np
from numba import njit, prange


@njit(parallel=True, fastmath=True)
def relative_relation_metric(vec1: np.array, vec2: np.array) -> int:

    if vec1.shape != vec2.shape:
        raise ValueError("The length of the vectors must be the same.")

    n = vec1.shape[0]
    tau = 0
    for i in prange(n - 1):
        for j in prange(i + 1, n):
            a = vec1[i] - vec1[j]
            b = vec2[i] - vec2[j]
            if a * b < 0:
                tau += 1
    return tau