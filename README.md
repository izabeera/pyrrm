# pyrrm

## Getting started


## Installation

### poetry
Extend the pyproject.toml by line:
```
[tool.poetry.dependencies]
python = "^3.12.1"
....
pyrrm = { git = "https://gitlab.com/izabeera/pyrrm.git", branch="main" }
...
```

## Usage

```
from pyrrm import relative_relation_metric
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import cross_val_score

X, y = load_data() # prepare dataframes with data 
clf = KNeighborsClassifier(n_neighbors=3, metric=relative_relation_metric)
print(cross_val_score(clf, X, y, cv=10))
```


## Authors and acknowledgment
Library used in "Relative Relation in KNN Classification for Gene Expression Data. A Preliminary Study"
- Izabela Kartowicz-Stolarska i.stolarska@pb.edu.pl
- Marcin Czajkowski m.czajkowski@pb.edu.pl
